﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEditor.Timeline;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class Animation : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private PlayableDirector animator;
    [SerializeField]
    private PlayableDirector mainAnimator;
    [SerializeField]
    private Text results;
    [SerializeField]
    private GameObject panel; 
    
    
    private Dictionary<int, double> tStud = new Dictionary<int, double>()
    {
        {50, 2.678}, {51, 2.676}, {52, 2.674}, {53, 2.672}, {54, 2.670}, {55, 2.688}, {56, 2.667}, {57, 2.665}, 
        {58, 2.663}, {59, 2.662}, {60, 2.660}, {61, 2.659}, {62, 2.657}, {63, 2.656}, {64, 2.655}, {65, 2.654},
        {66, 2.652}, {67, 2.651}, {68, 2.650}, {69, 2.649}, {70, 2.648}, {71, 2.647}, {72, 2.646}, {73, 2.645},
        {74, 2.644}, {75, 2.643}, {76, 2.642}, {77, 2.641}, {78, 2.640}, {79, 2.639}, {80, 2.639}, {81, 2.639},
        {82, 2.639}, {83, 2.639}, {84, 2.639}, {85, 2.639}, {86, 2.639}, {87, 2.639}, {88, 2.639}, {89, 2.639},
        {90, 2.632}, {91, 2.632}, {92, 2.632}, {93, 2.632}, {94, 2.632}, {95, 2.632}, {96, 2.632}, {97, 2.632} 
    };
    
    [SerializeField]
    private double needTime = 0;
    private string name;
    private Vector3 difference;
    private Vector3 alpha = new Vector3(1, 1, 1);

    class Mean
    {
        public double X;
        public double Y;
        public double Z;
        
        public Mean()
        {
            X = 0;
            Y = 0;
            Z = 0;
        }
    }

    class Std
    {
        public List<double> X;
        public List<double> Y;
        public List<double> Z;

        public Std()
        {
            X = new List<double>();
            Y = new List<double>();
            Z = new List<double>();
        }
    }

    private void Awake()
    {
        animator.time = 0;
        animator.Evaluate();
        mainAnimator.time = 0;
        mainAnimator.Evaluate();
        Vector3 main = mainAnimator.gameObject.transform.position;
        Vector3 current = animator.gameObject.transform.position;
        difference = new Vector3(main.x - current.x, main.y - current.y, main.z - current.z);
    }

    private List<string> bones = new List<string>()
    {
        "mixamorig:LeftUpLeg", "mixamorig:LeftLeg", "mixamorig:LeftFoot", "mixamorig:LeftToeBase", 
        "mixamorig:RightUpLeg", "mixamorig:RightLeg", "mixamorig:RightFoot", "mixamorig:RightToeBase",
        "mixamorig:Spine", "mixamorig:Spine1", "mixamorig:Spine2", 
        "mixamorig:LeftArm", "mixamorig:LeftForeArm", "mixamorig:LeftHand", "mixamorig:LeftHandMiddle1",
        "mixamorig:Neck", "mixamorig:Head", 
        "mixamorig:RightArm", "mixamorig:RightForeArm", "mixamorig:RightHand", "mixamorig:RightHandMiddle1"
    };
    
    [ContextMenu("Click me!?")]
    public void FindBonesDifference()
    {
        var builder = new StringBuilder();
        Transform obj = null, mainObj = null;
        int allPoints = 0;
        int rightPoints = 0;
        double accuracy = 0;
        var mean = new Mean();
        var main_mean = new Mean();
        var std = new Std();
        var main_std = new Std();
        double ssReg = 0;
        double ssRes = 0;
        foreach (var bone in bones)
        {
            name = bone;
            for (double time = 0; time < animator.duration; time += needTime)
            {
                allPoints++;
                animator.time = time;
                animator.Evaluate();
                mainAnimator.time = time;
                mainAnimator.Evaluate();
                
                obj = null;
                mainObj = null;
                obj = FindObject(animator.gameObject.transform, obj);
                mainObj = FindObject(mainAnimator.gameObject.transform, mainObj);
                if (obj == null || mainObj == null)
                {
                    Debug.Log("Null " + bone);
                    allPoints--;
                }
                
                Vector3 dif = mainObj.position - difference - obj.position;
                if (dif.x <= alpha.x && dif.y <= alpha.y && dif.z <= alpha.z)
                    rightPoints++;
                
                mean.X += obj.position.x;
                mean.Y += obj.position.y;
                mean.Z += obj.position.z;

                std.X.Add(obj.position.x);
                std.Y.Add(obj.position.y);
                std.Z.Add(obj.position.z);
                
                main_mean.X += obj.position.x;
                main_mean.Y += obj.position.y;
                main_mean.Z += obj.position.z;

                main_std.X.Add(obj.position.x);
                main_std.Y.Add(obj.position.y);
                main_std.Z.Add(obj.position.z);
            }

            mean.X /= allPoints; 
            mean.Y /= allPoints; 
            mean.Z /= allPoints; 
            
            main_mean.X /= allPoints; 
            main_mean.Y /= allPoints; 
            main_mean.Z /= allPoints; 
            for (int i = 0; i < std.X.Count; i++)
            {
                ssReg += ((std.X[i] - mean.X) * (std.X[i] - mean.X) + (std.Y[i] - mean.Y) * (std.Y[i] - mean.Y)
                                                                 + (std.Z[i] - mean.Z) * (std.Z[i] - mean.Z)) / 3;
                ssRes += ((main_std.X[i] - main_mean.X) * (main_std.X[i] - main_mean.X) 
                          + (main_std.Y[i] - main_mean.Y) * (main_std.Y[i] - main_mean.Y) 
                          + (main_std.Z[i] - main_mean.Z) * (main_std.Z[i] - main_mean.Z)) / 3;
            }

            ssReg /= allPoints;
            ssRes /= allPoints;
            
            accuracy = rightPoints / allPoints;
            builder.Append(bone.Replace("mixamorig:", "")).Append("\n");
            builder.Append("    Accuracy: ").Append(accuracy * 100).Append("%\n");
            builder.Append("    R2: ").Append(ssReg / (ssReg + ssRes) * 100).Append("%\n");
        }
        results.text = builder.ToString();
        panel.active = true;
    }

    public void Close()
    {
        panel.active = false;
    }

    public void CritStudent()
    {
        List<Vector3> positions = new List<Vector3>();
        List<Vector3> mainPositions = new List<Vector3>();
        var builder = new StringBuilder();
        foreach (var bone in bones)
        {
            name = bone;
            for (double time = 0; time < animator.duration; time += needTime)
            {
                animator.time = time;
                animator.Evaluate();
                mainAnimator.time = time;
                mainAnimator.Evaluate();

                Transform obj = null, mainObj = null;
                obj = FindObject(animator.gameObject.transform, obj);
                mainObj = FindObject(mainAnimator.gameObject.transform, mainObj);
                if (obj != null && mainObj != null)
                {
                    positions.Add(obj.position);
                    mainPositions.Add(mainObj.position);
                }
            }

            double x_percent, y_percent, z_percent;
            var x_t = Student(positions, mainPositions, out x_percent, vector => vector.x);
            var y_t = Student(positions, mainPositions, out y_percent, vector => vector.y);
            var z_t = Student(positions, mainPositions, out z_percent, vector => vector.z);
            var boneName = bone.Replace("mixamorig:", "");
            if (x_t && y_t && z_t)
            {
                builder.Append(boneName).Append(": Прекрасно!\n");
            }
            else
            {
                if (!x_t)
                    builder.Append(boneName).Append(": Ошибки в позиции x, ").Append(x_percent).Append("%\n");
                if (!y_t)
                    builder.Append(boneName).Append(": Ошибки в позиции x, ").Append(y_percent).Append("%\n");
                if (!z_t)
                    builder.Append(boneName).Append(": Ошибки в позиции x, ").Append(z_percent).Append("%\n");
            }
            positions.Clear();
            mainPositions.Clear();
        }
        results.text = builder.ToString();
        panel.active = true;
    }

    [ContextMenu("ManUitney")]
    public void CritMann()
    {
        List<Vector3> positions = new List<Vector3>();
        List<Vector3> mainPositions = new List<Vector3>();
        foreach (var bone in bones)
        {
            name = bone;
            for (double time = 0; time < animator.duration; time += needTime)
            {
                animator.time = time;
                animator.Evaluate();
                mainAnimator.time = time;
                mainAnimator.Evaluate();

                Transform obj = null, mainObj = null;
                obj = FindObject(animator.gameObject.transform, obj);
                mainObj = FindObject(mainAnimator.gameObject.transform, mainObj);
                if (obj != null && mainObj != null)
                {
                    positions.Add(obj.position);
                    mainPositions.Add(mainObj.position);
                }
            }
            var x_t = MannUitney(positions, mainPositions, 0.05, vector => vector.x);
            var y_t = MannUitney(positions, mainPositions, 0.05, vector => vector.y);
            var z_t = MannUitney(positions, mainPositions, 0.05, vector => vector.z);
          
            Debug.Log(bone + ":\n " + x_t);
            Debug.Log("\n " + y_t);
            Debug.Log("\n " + z_t);
            positions.Clear();
            mainPositions.Clear();
        }

    }

    bool Student(List<Vector3> pos, List<Vector3> mainPos, out double a, Func<Vector3, double> func)
    {
        int n1 = pos.Count;
        int n2 = mainPos.Count;
        
        double mean1 = pos.Sum(vector => func(vector)) / n1;
        double mean2 = mainPos.Sum(vector => func(vector)) / n2;
        double diffs1 = pos.Sum(vector => Math.Pow(func(vector) - mean1, 2));
        double diffs2 = mainPos.Sum(vector => Math.Pow(func(vector) - mean2, 2));

        diffs1 = Math.Pow(diffs1 / (n1 - 1), 0.5);
        diffs2 = Math.Pow(diffs2 / (n2 - 1), 0.5);

        double sd = Math.Sqrt(diffs1 * diffs1 * (n1 - 1) + diffs2 * diffs2 * (n2 - 1));
        var t_stat = Math.Abs(mean1 - mean2) / sd;
        
        var df = n1 + n2 - 1;
        var p_value = tStud[df];
        if (t_stat >= p_value)
        {
            a = p_value / t_stat * 100;
            return false;
        }
        if(t_stat == 0.0)
            a = 0;
        else
            a = t_stat / p_value * 100;
        return true;
    }

    class Value : IComparable
    {
        public Vector3 Score { get; set; }
        public double Rang { get; set; }

        public Value(Vector3 score, double rang)
        {
            Score = score;
            Rang = rang;
        }

        public int CompareTo(Vector3 obj)
        {
            if (obj.x == Score.x && obj.y == Score.y && obj.z == Score.z)
                return 0;
            return 1;
        }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }

    double MannUitney(List<Vector3> pos, List<Vector3> mainPos, double a, Func<Vector3, double> func)
    {
        int n1 = pos.Count;
        int n2 = mainPos.Count;
        var allPos = pos;
        allPos.AddRange(mainPos);
        int counter = 0;
        var res = allPos.GroupBy(g => g)
         .OrderBy(o => o.Key)
         .Select(s => { counter++; return new { Score = s.Key, Rang = (double)counter / s.Count() }; })
         .Join(allPos, o => o.Score, i => i, (o, i) => new { Score = i, Rang = o.Rang })
         .Select(x => new Value(x.Score, x.Rang)).ToList();
        var pD = new List<Value>();
        foreach (var value in pos)
            pD.Add(new Value(value, res.First(y => y.CompareTo(value) == 0).Rang));
        var mainPosDic = new List<Value>();
        foreach (var value in pos)
            mainPosDic.Add(new Value(value, res.First(y => y.CompareTo(value) == 0).Rang));
        var sum1 = 0.0;
        foreach (var value in pD)
            sum1 += value.Rang;
        var sum2 = 0.0;
        foreach (var value in mainPosDic)
            sum2 += value.Rang;
        var t_stat = sum1;
        var n = n1;
        if(sum2 > sum1)
        {
            t_stat = sum2;
            n = n2;
        }
        var u = n1 * n2 + n * (n + 1) / 2 - t_stat;
        return u;
    }

    Transform FindObject(Transform transform, Transform returnObj)
    {
        if (returnObj != null)
            return returnObj;
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            if (child.name.Equals(name))
                return child;
            if (child.childCount != 0)
            {
                var obj = child.Find(name);
                
                if (obj == null)
                    returnObj = FindObject(child, returnObj);
                else
                    return obj;
            }
        }
        return returnObj;
    }
}
